using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System;
using Firebase;
using Firebase.Database;
using Firebase.Storage;
using Firebase.Extensions;
using Firebase.Firestore;
using Firebase.Analytics;

	/*Copyright (C) 2021  Reboot Rebels( Nick Salvi, Trevor Blanchard, Ryan Cote)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see https://www.gnu.org/licenses/.
	*/
public class FetchScene : MonoBehaviour
{
    //IOS Links, uncomment for building on IOS
    
    /*
    private string[] links = { "https://firebasestorage.googleapis.com/v0/b/rebootrebelsar-7ba54.appspot.com/o/scene1?alt=media&token=0797c2b5-c5e6-496b-a768-ce968ba6d8f7",
                               "https://firebasestorage.googleapis.com/v0/b/rebootrebelsar-7ba54.appspot.com/o/scene1?alt=media&token=0797c2b5-c5e6-496b-a768-ce968ba6d8f7",
                               "https://firebasestorage.googleapis.com/v0/b/rebootrebelsar-7ba54.appspot.com/o/scene2?alt=media&token=d87960ea-4475-4836-9cb0-127a0e20c06c",
                               "https://firebasestorage.googleapis.com/v0/b/rebootrebelsar-7ba54.appspot.com/o/scene1?alt=media&token=0797c2b5-c5e6-496b-a768-ce968ba6d8f7",
                               "https://firebasestorage.googleapis.com/v0/b/rebootrebelsar-7ba54.appspot.com/o/scene3?alt=media&token=d694cc03-1369-4a35-80c7-1505c751b9a6",
                               "SPECIAL_CASE_IGNORE",
                               "https://firebasestorage.googleapis.com/v0/b/rebootrebelsar-7ba54.appspot.com/o/scene1?alt=media&token=0797c2b5-c5e6-496b-a768-ce968ba6d8f7"
                              // "https://firebasestorage.googleapis.com/v0/b/rebootrebelsar-7ba54.appspot.com/o/scene4?alt=media&token=8efc3cc8-a0d3-4fbc-b27f-19e4230d36cc"
    }; */

    //Android Links, uncomment for building on Android

    
    private string[] links = { "https://firebasestorage.googleapis.com/v0/b/rebootrebelsar-7ba54.appspot.com/o/scene1Android?alt=media&token=b93afd86-4de4-46ae-b0ae-7ccc00405b71",
                               "https://firebasestorage.googleapis.com/v0/b/rebootrebelsar-7ba54.appspot.com/o/scene1Android?alt=media&token=b93afd86-4de4-46ae-b0ae-7ccc00405b71",
                               "https://firebasestorage.googleapis.com/v0/b/rebootrebelsar-7ba54.appspot.com/o/scene2Android?alt=media&token=6c12b739-a80d-4bbb-98ab-4b243c5b2444",
                               "https://firebasestorage.googleapis.com/v0/b/rebootrebelsar-7ba54.appspot.com/o/scene1Android?alt=media&token=b93afd86-4de4-46ae-b0ae-7ccc00405b71",
                               "https://firebasestorage.googleapis.com/v0/b/rebootrebelsar-7ba54.appspot.com/o/scene3Android?alt=media&token=4b4f0abc-cc54-489a-afb1-af10c3154e5b",
                               "SPECIAL_CASE_IGNORE",
                               "https://firebasestorage.googleapis.com/v0/b/rebootrebelsar-7ba54.appspot.com/o/scene1Android?alt=media&token=b93afd86-4de4-46ae-b0ae-7ccc00405b71"
    };

    private AssetBundle currbundle;                   
                               
    public void beginSceneChange(int sceneToChange, GameObject Driver)
    {
        StartCoroutine(changeScene(sceneToChange, Driver));
     }

    public IEnumerator changeScene(int sceneToChange, GameObject Driver)
	{
		Debug.Log("Inside changeScene");         
        UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(links[sceneToChange]);
        yield return www.SendWebRequest();
 
        if (www.result != UnityWebRequest.Result.Success) {
            Debug.Log(www.error);
        }
        else {

            if (currbundle == null)
            {
                currbundle = DownloadHandlerAssetBundle.GetContent(www);
            }
            else
            {
                currbundle.Unload(false);
                currbundle = DownloadHandlerAssetBundle.GetContent(www);
            }
            string[] scenePaths = currbundle.GetAllScenePaths();
			string sceneName = System.IO.Path.GetFileNameWithoutExtension(scenePaths[0]);
            DontDestroyOnLoad(Driver);

            SceneManager.LoadScene(sceneName);
        }
        

    }
}
