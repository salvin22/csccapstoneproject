using Firebase;
using Firebase.Database;
using Firebase.Storage;
using Firebase.Extensions;
using Firebase.Firestore;
using Firebase.Analytics;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;

	/*Copyright (C) 2021  Reboot Rebels( Nick Salvi, Trevor Blanchard, Ryan Cote)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see https://www.gnu.org/licenses/.
	*/
	
public class SpawnMaterial : MonoBehaviour
{
    //public GameObject Mack2DoubleSided;
    public Material tempMaterial;


    void Start()
    {
        Firebase.Storage.FirebaseStorage storage = Firebase.Storage.FirebaseStorage.DefaultInstance;
        Firebase.Storage.StorageReference storage_ref = storage.GetReferenceFromUrl("gs://rebootrebelsar-7ba54.appspot.com");
        byte[] fileContents = null;
        storage_ref.Child("NewBranch.png").GetBytesAsync(2048 * 2048)
             .ContinueWithOnMainThread((Task<byte[]> task) =>
             {
                 if (task.IsFaulted || task.IsCanceled)
                 {
                     Debug.Log(task.Exception.ToString());
                 }
                 else
                 {
                     byte[] fileContents = task.Result;
                     Debug.Log("Load Image after getting result!");
                     Texture2D texture = new Texture2D(2048, 2048);
                     texture.LoadImage(fileContents);
                     tempMaterial.SetTexture("_MainTex", texture);
                     //Mack2DoubleSided.GetComponent<Renderer>().sharedMaterial = tempMaterial;
                     Debug.Log("finished");
                 }
             });
        Texture2D texture = new Texture2D(2048, 2048);
        texture.LoadImage(fileContents);
        tempMaterial.SetTexture("_MainTex", texture);
    }
}
