using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
	/*Copyright (C) 2021  Reboot Rebels( Nick Salvi, Trevor Blanchard, Ryan Cote)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see https://www.gnu.org/licenses/.
	*/
public class MenuOpener : MonoBehaviour
{
    // Start is called before the first frame update
    //This could probably be cleaned up, take a buttons name as input, and make some generic methods / switch statement to make it cleaner
   // public GameObject menuCanvas;
    public GameObject menuObject;
    public GameObject creditsObject;
    public GameObject seatObject;
    public GameObject pointObject;
    public GameObject waitObject;
    public GameObject driverObject;

    public void openMenu()
    {

        if (menuObject != null)
        {
            //menuCanvas.SetActive(true); disable gameObject attached
            menuObject.GetComponent<Canvas>().enabled = true;
        }

    }

    public void closeMenu()
    {

        if (menuObject != null)
        {
            menuObject.GetComponent<Canvas>().enabled = false;
        }

    }

    public void creditsReturn()
    {

        if (creditsObject != null)
        {
            creditsObject.GetComponent<Canvas>().enabled = false;
            openMenu();
        }

    }

    public void mainMenuToCredits()
    {

        if (creditsObject != null)
        {
            closeMenu();
            creditsObject.GetComponent<Canvas>().enabled = true;
        }

    }

    public void mainMenuToSeats()
    {

        if (seatObject != null)
        {
            closeMenu();
            seatObject.GetComponent<Canvas>().enabled = true;
        }

    }

    public void seatsToMainMenu()
    {

        if (seatObject != null)
        {
            seatObject.GetComponent<Canvas>().enabled = false;
            openMenu();
        }

    }

    public void pointClose()
    {

        if (pointObject != null)
        {
            pointObject.GetComponent<Canvas>().enabled = false;
        }

    }

    public void pointToWait()
    {

        if (pointObject != null)
        {
            pointObject.GetComponent<Canvas>().enabled = false;
            waitObject.GetComponent<Canvas>().enabled = true;
            driverObject.GetComponent<FirebaseListener>().enabled = true;


        }

    }
}
