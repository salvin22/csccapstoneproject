using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

	/*Copyright (C) 2021  Reboot Rebels( Nick Salvi, Trevor Blanchard, Ryan Cote)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see https://www.gnu.org/licenses/.
	*/
	
public class SeatSelectHandler : MonoBehaviour
{
    // Start is called before the first frame update
    //This could probably be cleaned up, take a buttons name as input, and make some generic methods / switch statement to make it cleaner
   // public GameObject menuCanvas;
    public GameObject menuObject;
    public GameObject seatObject;
    public GameObject pointObject;

    public ARSessionOrigin pos;

    public void seatSelectionHandler(int seatNum)
    {
        Debug.Log(seatNum);
        switch (seatNum)
        {
            case 1:
                GlobalVariables.seatPosition = 1;
                pos.transform.position = new Vector3(3.0f, 1.5f, 1.5f);
                seatObject.GetComponent<Canvas>().enabled = false;
                pointObject.GetComponent<Canvas>().enabled = true;

                break;
            case 2:
                GlobalVariables.seatPosition = 2;
                pos.transform.position = new Vector3(0, 1.5f, 1.5f);
                seatObject.GetComponent<Canvas>().enabled = false;
                pointObject.GetComponent<Canvas>().enabled = true;
                break;
            case 3:
                GlobalVariables.seatPosition = 3;
                pos.transform.position = new Vector3(-3.0f, 1.5f, 1.5f);
                seatObject.GetComponent<Canvas>().enabled = false;
                pointObject.GetComponent<Canvas>().enabled = true;
                break;
            default:
                GlobalVariables.seatPosition = 2;
                pos.transform.position = new Vector3(0, 1.5f, 1.5f);
                seatObject.GetComponent<Canvas>().enabled = false;
                pointObject.GetComponent<Canvas>().enabled = true;
                break;
        }
    }
}
